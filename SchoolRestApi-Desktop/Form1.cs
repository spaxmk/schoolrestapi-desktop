﻿using SchoolRestApi_Desktop.Helpers;
using SchoolRestApi_Desktop.Models;
using SchoolRestApi_Desktop.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolRestApi_Desktop
{
    public partial class Form1 : Form
    {
        //public static APIHelper _apiHelper;
        private Form currentChildForm;

        public Form1()
        {
            InitializeComponent();
            lblWelcomeBack.Text += APIHelper.loggedUser.UserName;
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            Form login = new LoginForm();
            login.Show();
        }

        private void btnCloseApp_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void PanelNav_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void OpenChildForm(Form childForm)
        {
            if (currentChildForm != null)
            {
                currentChildForm.Close();
            }
            currentChildForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            PanelForm.Controls.Add(childForm);
            PanelForm.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();

        }

        private void btnSchools_Click(object sender, EventArgs e)
        {
            OpenChildForm(new SchoolForm());
        }

        private void panelImg_MouseClick(object sender, MouseEventArgs e)
        {
            if (currentChildForm != null)
            {
                currentChildForm.Close();
            }
        }
    }
}
