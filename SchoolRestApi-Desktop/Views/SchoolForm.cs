﻿using SchoolRestApi_Desktop.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolRestApi_Desktop.Views
{
    public partial class SchoolForm : Form
    {
        
        public SchoolForm()
        {
            InitializeComponent();
            LoadDataAsync();
        }

        private async Task LoadDataAsync()
        {
            dataGridView1.Update();
            var dataList = await APIHelper.GetAll();
            dataGridView1.DataSource = dataList;
        }

        private async void btnRefreshData_Click(object sender, EventArgs e)
        {
            await LoadDataAsync();
        }
    }
}
