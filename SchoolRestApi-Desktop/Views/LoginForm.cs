﻿using SchoolRestApi_Desktop.Helpers;
using SchoolRestApi_Desktop.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolRestApi_Desktop.Views
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            
        }

        private string _userName;
        private string _password;
        
        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
            }
        }

        public bool CanLogIn
        {
            get
            {
                bool output = false;
                if(UserName?.Length > 0 && Password?.Length > 0 && UserName != "Username:" && Password != "Password:")
                {
                    output = true;
                }
                return output;
            }
        }


        public async Task LogIn(string username, string password)
        {
            var result = await APIHelper.Authenticate(username, password);
            if (result.UserName != null)
            {
                Form1 form1 = new Form1();
                form1.Show();
                this.Hide();
            }
            else
            {
                lblFaildLogin.ForeColor = Color.OrangeRed;
                lblFaildLogin.Text = "BadRequest";
            }
        }

        private async void btnLogIn_Click(object sender, EventArgs e)
        {
            lblFaildLogin.ForeColor = Color.DarkOliveGreen;
            lblFaildLogin.Text = "Molimo sacekajte ..";

            _userName = txtUsername.Text.ToString();
            _password = txtPassword.Text.ToString();

            if (CanLogIn) 
            {
                try
                {
                    await LogIn(_userName, _password);
                }
                catch (Exception)
                {
                    lblFaildLogin.ForeColor = Color.OrangeRed;
                    lblFaildLogin.Text = "Korisnicko ime ili sifra nije ispravno uneseno.";
                    //throw;
                }
            }
            else
            {
                lblFaildLogin.ForeColor = Color.OrangeRed;
                lblFaildLogin.Text = "Email i Sifra su obavezni!";
            }
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void PanelNav_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            if(txtPassword.Text == "Password:")
                txtPassword.Text = "";
        }

        private void txtPassword_Leave(object sender, EventArgs e)
        {
            if (txtPassword.Text == "")
            {
                txtPassword.Text = "Password:";
                lblFaildLogin.ForeColor = Color.OrangeRed;
                lblFaildLogin.Text = "Sifra je obavezna!";
            }
        }

        private void txtUsername_Enter(object sender, EventArgs e)
        {
            if (txtUsername.Text == "Username:")
                txtUsername.Text = "";
        }

        private void txtUsername_Leave(object sender, EventArgs e)
        {
            if (txtUsername.Text == "")
            {
                txtUsername.Text = "Username:";
                lblFaildLogin.ForeColor = Color.OrangeRed;
                lblFaildLogin.Text = "Korisnicko ime je obavezno!";
            }

        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            txtPassword.PasswordChar = '*';
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            //Application.Exit();
            this.Dispose();
        }
    }
}
