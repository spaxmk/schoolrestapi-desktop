﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolRestApi_Desktop.Models
{
    public class School
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Adresa { get; set; }
        public string Grad { get; set; }
        public string Kontakt { get; set; }
        public int GodinaRada { get; set; }
    }
}
