﻿using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace SchoolRestApi_Desktop.Models
{
    public class AuthenticatedUser
    {
        public string Access_Token { get; set; }
        public string UserName { get; set; }

        public AuthenticatedUser(string Access_Token, string UserName)
        {
            this.Access_Token = Access_Token;
            this.UserName = UserName;
        }
    }
}
