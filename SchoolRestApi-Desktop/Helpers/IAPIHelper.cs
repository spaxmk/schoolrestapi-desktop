﻿using SchoolRestApi_Desktop.Models;
using System.Threading.Tasks;

namespace SchoolRestApi_Desktop.Helpers
{
    public interface IAPIHelper
    {
        Task<AuthenticatedUser> Authenticate(string username, string password);
    }
}