﻿using Newtonsoft.Json;
using RestSharp;
using SchoolRestApi_Desktop.Models;
using SchoolRestApi_Desktop.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Pipes;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolRestApi_Desktop.Helpers
{
    public static class APIHelper 
    {
        public static HttpClient apiClient;
        static HttpClient client = new HttpClient();
        public static bool RequestStatus;
        public static AuthenticatedUser loggedUser;
        public static string baseUrl;
        
        public static void InitializeClient()
        {
            string api = ConfigurationManager.AppSettings["api"];
            apiClient = new HttpClient();
            apiClient.BaseAddress = new Uri(api);
            if (loggedUser!=null)
            {
                apiClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + loggedUser.Access_Token);  
            }
            apiClient.DefaultRequestHeaders.Accept.Clear();
            apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //var client = new RestClient(new Uri(api));
            //client.Timeout = -1;

        }

        public static async Task<List<School>> GetAll()
        {

            //var authorize = new Dictionary<string, string>
            //{
            //    { "Authorization",  loggedUser.Access_Token }
            //};
            //var authorizeData = new FormUrlEncodedContent(authorize);
            //var authorizeDatas = new HttpCompletionOption();

            //using (HttpResponseMessage response = await apiClient.GetAsync("/api/schools/details/" /* , HttpCompletionOption */ ))
            //{
            //    if (response.IsSuccessStatusCode)
            //    {
            //        var result = await response.Content.ReadAsStringAsync();
            //        if (result != null)
            //        {
            //            var listData = JsonConvert.DeserializeObject<List<School>>(result);
            //            return listData;
            //        }
            //    }
            //}
            //return null;

            var request = new RestRequest(Method.GET);
            //var client = new RestClient(baseUrl + "/api/schools/details");
            var client = new RestClient(CreateApiUrl("/api/schools/details/"));
            client.Timeout = -1;
            request.AddHeader("Authorization", "Bearer " + loggedUser.Access_Token);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            var listData = JsonConvert.DeserializeObject<List<School>>(response.Content);
            return listData;
        }

        public static string CreateApiUrl(string apiUrl)
        {
            baseUrl = ConfigurationManager.AppSettings["api"];
            return baseUrl += apiUrl;
        }

        public static async Task<AuthenticatedUser> Authenticate(string username, string password)
        {
            InitializeClient();
            var inputData = new Dictionary<string, string>
            {
                {"grant_type", "password" },
                {"username", username },
                {"password", password }
            };
            var data = new FormUrlEncodedContent(inputData);

            //var data = new FormUrlEncodedContent(new[]
            //{
            //    new KeyValuePair<string, string>("grant_type", "password"),
            //    new KeyValuePair<string, string>("username", username),
            //    new KeyValuePair<string, string>("password", password),
            //});

            using (HttpResponseMessage response = await apiClient.PostAsync("/Token", data))
            {
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsAsync<AuthenticatedUser>();
                    loggedUser = new AuthenticatedUser(result.Access_Token, result.UserName);
                    return result;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        

    }
}
