﻿namespace SchoolRestApi_Desktop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCloseApp = new System.Windows.Forms.Button();
            this.PanelMenu = new System.Windows.Forms.Panel();
            this.btnProfesori = new System.Windows.Forms.Button();
            this.btnSchools = new System.Windows.Forms.Button();
            this.PanelLogo = new System.Windows.Forms.Panel();
            this.panelImg = new System.Windows.Forms.Panel();
            this.PanelNav = new System.Windows.Forms.Panel();
            this.PanelForm = new System.Windows.Forms.Panel();
            this.lblWelcomeBack = new System.Windows.Forms.Label();
            this.PanelMenu.SuspendLayout();
            this.PanelLogo.SuspendLayout();
            this.PanelNav.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCloseApp
            // 
            this.btnCloseApp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseApp.BackColor = System.Drawing.Color.DarkGray;
            this.btnCloseApp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseApp.Location = new System.Drawing.Point(677, 3);
            this.btnCloseApp.Name = "btnCloseApp";
            this.btnCloseApp.Size = new System.Drawing.Size(23, 22);
            this.btnCloseApp.TabIndex = 1;
            this.btnCloseApp.Text = "X";
            this.btnCloseApp.UseVisualStyleBackColor = false;
            this.btnCloseApp.Click += new System.EventHandler(this.btnCloseApp_Click);
            // 
            // PanelMenu
            // 
            this.PanelMenu.BackColor = System.Drawing.Color.Silver;
            this.PanelMenu.Controls.Add(this.btnProfesori);
            this.PanelMenu.Controls.Add(this.btnSchools);
            this.PanelMenu.Controls.Add(this.PanelLogo);
            this.PanelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelMenu.Location = new System.Drawing.Point(0, 0);
            this.PanelMenu.Name = "PanelMenu";
            this.PanelMenu.Size = new System.Drawing.Size(200, 549);
            this.PanelMenu.TabIndex = 2;
            // 
            // btnProfesori
            // 
            this.btnProfesori.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProfesori.FlatAppearance.BorderSize = 0;
            this.btnProfesori.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfesori.Font = new System.Drawing.Font("Times New Roman", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProfesori.Location = new System.Drawing.Point(0, 198);
            this.btnProfesori.Name = "btnProfesori";
            this.btnProfesori.Size = new System.Drawing.Size(200, 40);
            this.btnProfesori.TabIndex = 3;
            this.btnProfesori.Text = "Profesori";
            this.btnProfesori.UseVisualStyleBackColor = true;
            // 
            // btnSchools
            // 
            this.btnSchools.BackColor = System.Drawing.Color.Transparent;
            this.btnSchools.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSchools.FlatAppearance.BorderSize = 0;
            this.btnSchools.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSchools.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSchools.ForeColor = System.Drawing.SystemColors.InfoText;
            this.btnSchools.Location = new System.Drawing.Point(0, 158);
            this.btnSchools.Name = "btnSchools";
            this.btnSchools.Size = new System.Drawing.Size(200, 40);
            this.btnSchools.TabIndex = 2;
            this.btnSchools.Text = "Skole";
            this.btnSchools.UseVisualStyleBackColor = false;
            this.btnSchools.Click += new System.EventHandler(this.btnSchools_Click);
            // 
            // PanelLogo
            // 
            this.PanelLogo.Controls.Add(this.panelImg);
            this.PanelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelLogo.Location = new System.Drawing.Point(0, 0);
            this.PanelLogo.Name = "PanelLogo";
            this.PanelLogo.Size = new System.Drawing.Size(200, 158);
            this.PanelLogo.TabIndex = 1;
            // 
            // panelImg
            // 
            this.panelImg.BackgroundImage = global::SchoolRestApi_Desktop.Properties.Resources.icon_school_5;
            this.panelImg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelImg.Location = new System.Drawing.Point(18, 7);
            this.panelImg.Name = "panelImg";
            this.panelImg.Size = new System.Drawing.Size(164, 143);
            this.panelImg.TabIndex = 0;
            this.panelImg.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelImg_MouseClick);
            // 
            // PanelNav
            // 
            this.PanelNav.BackColor = System.Drawing.Color.DarkGray;
            this.PanelNav.Controls.Add(this.lblWelcomeBack);
            this.PanelNav.Controls.Add(this.btnCloseApp);
            this.PanelNav.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelNav.Location = new System.Drawing.Point(200, 0);
            this.PanelNav.Name = "PanelNav";
            this.PanelNav.Size = new System.Drawing.Size(704, 56);
            this.PanelNav.TabIndex = 3;
            this.PanelNav.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelNav_MouseDown);
            // 
            // PanelForm
            // 
            this.PanelForm.AutoSize = true;
            this.PanelForm.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PanelForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelForm.Location = new System.Drawing.Point(200, 56);
            this.PanelForm.Name = "PanelForm";
            this.PanelForm.Size = new System.Drawing.Size(704, 493);
            this.PanelForm.TabIndex = 4;
            // 
            // lblWelcomeBack
            // 
            this.lblWelcomeBack.AutoSize = true;
            this.lblWelcomeBack.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcomeBack.Location = new System.Drawing.Point(6, 20);
            this.lblWelcomeBack.Name = "lblWelcomeBack";
            this.lblWelcomeBack.Size = new System.Drawing.Size(79, 17);
            this.lblWelcomeBack.TabIndex = 4;
            this.lblWelcomeBack.Text = "Dobro dosli ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 549);
            this.Controls.Add(this.PanelForm);
            this.Controls.Add(this.PanelNav);
            this.Controls.Add(this.PanelMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "RestApi";
            this.PanelMenu.ResumeLayout(false);
            this.PanelLogo.ResumeLayout(false);
            this.PanelNav.ResumeLayout(false);
            this.PanelNav.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCloseApp;
        private System.Windows.Forms.Panel PanelMenu;
        private System.Windows.Forms.Panel PanelNav;
        private System.Windows.Forms.Panel PanelForm;
        private System.Windows.Forms.Panel PanelLogo;
        private System.Windows.Forms.Panel panelImg;
        private System.Windows.Forms.Button btnProfesori;
        private System.Windows.Forms.Button btnSchools;
        private System.Windows.Forms.Label lblWelcomeBack;
    }
}

